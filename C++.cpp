﻿#include <iostream>
#include <string>

using namespace std;

class Account
{
private:
    int accountNumber;
    string bankName;
    string accountType;

public:
    Account(string name = "", string accountT = "", int accountN = 0)
    {
        bankName = name;
        accountNumber = accountN;
        accountType = accountT;
    };
    void setName(string theName)
    {
        this->bankName = theName;
    }
    void setAccountNumber(int theAccount)
    {
        this->accountNumber = theAccount;
    }
    void setAccountType(string theTyp)
    {
        this->accountType = theTyp;
    }
    string getBankName()
    {
        return bankName;
    }
    string getAccountType()
    {
        return accountType;
    }
    int getAccountNumber()
    {
        return accountNumber;
    }
    void display()
    {
        cout << "\nThe details of the Account is..";
        cout << "\nName : " << bankName << endl;
        cout << "\nAccount Number : " << accountNumber << endl;
        cout << "\nAccount Type : " << accountType << endl;
    };
    ~Account()
    {
        cout << "Account destroyed!" << endl;
    }
};
class PersonalAccount : public Account
{
private:
    string accountHolderName;
    int accountBalance;

public:
    PersonalAccount(string n = "", string at = "", int accountN = 0, string accountH = "", int accountB = 0)
        : Account(n, at, accountN)
    {
        this->accountHolderName = accountH;
        this->accountBalance = accountB;
    }
    void display_s()
    {
        Account::display();
        cout << "\nAccount Holder : " << accountHolderName << endl;
        cout << "\nAccount Balance : " << accountBalance << endl;
    }
    void setAccountHolder(string theNameD)
    {
        this->accountHolderName = theNameD;
    }
    void setAccountB(int theAccountB)
    {
        this->accountBalance = theAccountB;
    }
    ~PersonalAccount()
    {
        cout << "Account destroyed!" << endl;
    }

    int getAccountB()
    {
        return accountBalance;
    };
    string getAccountHolderName()
    {
        return accountHolderName;
    }
};
int main()
{
    PersonalAccount* arr = new PersonalAccount[50]; // dynamic array
    string n;
    int aco;
    string typ;
    int bah;
    string han;
    int saving = 0;
    int cheking = 0;
    int b = 0;
    string theCom;
    int checkH = 0;
    cout << "Enter data:" << endl;
    for (int i = 0; i < 50; i++)
    {
        cout << "Enter the Name of the bank : ";
        getline(cin, n);
        arr[i].setName(n);
        cout << "Enter the Account Number : ";
        cin >> aco;
        arr[i].setAccountNumber(aco);
        cin.ignore();
        cout << "SEnter the Account Type :(Savings/Checking): ";
        cin >> typ;
        arr[i].setAccountType(typ);
        cin.ignore();
        cout << "SEnter the Depositor`s Name : ";
        getline(cin, han);
        arr[i].setAccountHolder(han);
        cout << "SEnter the account balance:(whole number) : ";
        cin >> bah;
        arr[i].setAccountB(bah);
        cin.ignore();
    }
    cout << "Display data:" << endl;
    for (int i = 0; i < 50; i++)
    {
        arr[i].display_s();
    };


    cout << "\nSearch by Name:";
    getline(cin, theCom);
   /* cin >> theCom;*/
    for (int i = 0; i < 50; i++)
    {
        if (arr[i].getAccountHolderName()==theCom)
        {
            cout << "Account found:\n ";
            arr[i].display_s();
            checkH++;
            break;
        }
    }
    if (checkH == 0) {
        cout << "No account with name:" << theCom;
    }
    
    for (int i = 0; i < 50; i++)
    {
        b = b + arr[i].getAccountB();
        if (arr[i].getAccountType() == "Savings")
        {
            saving++;
        }
        if (arr[i].getAccountType() == "Checking")
        {
            cheking++;
        }
    };
    cout << "\n Information about the program " << saving << endl;
    cout << "Savings account number  = " << saving << endl;
    cout << "Checking account number  = " << cheking << endl;
    cout << "Sotal amount in them = " << b << endl;
    delete[] arr;

    return 0;
}


